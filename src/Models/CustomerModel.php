<?php

namespace Bookstore\Models;

use Bookstore\Domain\Customer;
use Bookstore\Domain\Customer\CustomerFactory;
use Bookstore\Exceptions\NotFoundException;

class CustomerModel extends AbstractModel
{
    public function get(int $userId): Customer
    {
        $query = 'SELECT * FROM customer WHERE id = :user';
        $sth = $this->db->prepare($query);
        $sth->execute(['user' => $userId]);
        $row = $sth->fetch();
        if (empty($row)) {
            throw new NotFoundException('Not fount customer');
        }
        return CustomerFactory::factory(
            $row['TYPE'],
            $row['id'],
            $row['firstname'],
            $row['surname'],
            $row['email']
        );
    }

    public function getByEmail(string $email): Customer
    {
        $query = 'SELECT * FROM customer WHERE email = :user';
        $sth = $this->db->prepare($query);
        $sth->execute(['user' => $email]);
        $row = $sth->fetch();
        if (empty($row)) {
            throw new NotFoundException('Rows Empty');
        }
        return CustomerFactory::factory(
            $row['TYPE'],
            $row['id'],
            $row['firstname'],
            $row['surname'],
            $row['email']
        );
    }
}
