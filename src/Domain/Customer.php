<?php

namespace Bookstore\Domain;

use Bookstore\Domain\Payer;

interface Customer extends Payer 
{
    public function getMonthlyFee(): float;
    public function getAmountToBorrow(): int;
    public function getType(): string;
}
