<?php

namespace Bookstore\Controllers;

class ErrorController extends AbstractController
{
    public function notFound(): string
    {
        $properties = ['errorMessage' => 'La ruta no existe'];
        return $this->render('error.twig', $properties);
    }
}
