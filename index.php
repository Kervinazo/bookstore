<?php

use Bookstore\Core\Router;
use Bookstore\Core\Request;

require_once __DIR__ . '/vendor/autoload.php';

$router = new Router();
$response = $router->route(new Request());
echo $response;

/*$loader = new Twig_Loader_Filesystem(__DIR__ . '/views');
$twig = new Twig_Environment($loader);

$bookModel = new BookModel(Db::getInstance());
$books = $bookModel->getAll(1, 4);
$params = ['books' => $books, 'currentPage' => 2];
echo $twig->loadTemplate('books.twig')->render($params);

$saleModel = new SaleModel(Db::getInstance());
$sales = $saleModel->getByUser(1);
var_dump($sales);
$params = ['sales' => $sales];
echo $twig->loadTemplate('sales.twig')->render($params);

$saleModel = new SaleModel(Db::getInstance());
$sale = $saleModel->get(5);

$params = ['sale' => $sale];
echo $twig->loadTemplate('sale.twig')->render($params);*/
