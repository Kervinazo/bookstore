<?php

use Bookstore\Domain\Book;
use Bookstore\Domain\Customer;
use Bookstore\Domain\Customer\Basic;
use Bookstore\Domain\Customer\Premium;
use Bookstore\Domain\Payer;
use Bookstore\Exceptions\ExceededMaxAllowedException;
use Bookstore\Exceptions\InvalidIdException;
use Bookstore\Domain\Customer\CustomerFactory;
use Bookstore\Core\Config;


function autoloader($classname)
{
    $lastSlash = strpos($classname, '\\') + 1;
    $classname = substr($classname, $lastSlash);
    $directory = str_replace('\\', '/', $classname);
    $filename = __DIR__ . '/src/' . $directory . '.php';
    require_once($filename);
}

spl_autoload_register('autoloader');


$book = new Book("1984", "George Orwell", 9785267006323, 12);
echo $string = (string) $book; // title - author Not available

$customer = new Basic(2, 'John', 'Doe', 'johndoe@mail.com');
echo $customer->getAmountToBorrow();

$basic = new Basic(3, "name", "surname", "email");
$premium = new Premium((int)null, "name", "surname", "email");
var_dump($basic->getId()); // 1
var_dump($premium->getId()); // 2

function checkIfValid(Customer $customer, array $books): bool
{
    return $customer->getAmountToBorrow() >= count($books);
}

var_dump(checkIfValid($customer, [$book])); // ok


if ($book->getCopy()) {
    echo '<i>Here, your copy.</i><hr>';
} else {
    echo 'I am afraid that book is not available.';
}


try {
    $basic = new Basic(-1, "name", "surname", "email");
} catch (Exception $e) {
    echo 'Something happened when creating the basic customer: ' . $e->getMessage();
}


function createBasicCustomer(int $id)
{
    try {
        echo "\nTrying to create a new customer with id $id.\n";
        return CustomerFactory::factory('premium', $id, 'james', 'bond', 'james@
        bond.com');
    } catch (Exception $e) {
        echo 'Unknown exception: ' . $e->getMessage() . "\n";
    } catch (InvalidIdException $e) {
        echo "You cannot provide a negative id.\n";
    } catch (ExceededMaxAllowedException $e) {
        echo "No more customers are allowed.\n";
    }
}

$payer = createBasicCustomer(3);

function processPayment(Payer $payer, float $amount)
{
    if ($payer->isExtentOfTaxes()) {
        echo "What a lucky one...";
    } else {
        $amount *= 1.16;
    }
    $payer->pay($amount);
}

echo processPayment($payer, 1.5);



$config = Config::getInstance();
$dbConfig = $config->get('db');

$books = [
    ['title' => '1984', 'price' => 8.15],
    ['title' => 'Don Quijote', 'price' => 12.00],
    ['title' => 'Odyssey', 'price' => 3.55]
];



$addTaxes = function (array &$book, $index, $percentage) {
    $book['price'] += round($percentage * $book['price'], 2);
};

array_walk($books, $addTaxes, 0.16);

$dbConfig = Config::getInstance()->get('db');
try {
    $db = new PDO(
        'mysql:host=127.0.0.1;dbname=bookslave',
        $dbConfig['user'],
        $dbConfig['password']
    );
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (\Exception $e) {
    echo "Error en la conexion" . $e->getMessage();
}

$query = 'SELECT * FROM book WHERE author = :author';
$statement = $db->prepare($query);
$statement->bindValue('author', 'George Orwell');
$statement->execute();
$rows = $statement->fetchAll();

foreach ($rows as $key => $row) {
    $string = $row['title'];
    echo '<li>' . $string . '</li>';
}
